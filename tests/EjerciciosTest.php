<?php

namespace App\Tests;

use App\Ejercicios;
use PHPUnit\Framework\TestCase;

class EjerciciosTest extends TestCase
{
    /** @test */
    public function filtrarPerros()
    {
        $ejercicio = new Ejercicios();

        $perros = [
            ["name" => "Leo", "color" => "negro"],
            ["name" => "Turrón", "color" => "blanco"],
            ["name" => "Nevado", "color" => "marrón"],
            ["name" => "Mr. N", "color" => "blanco"],
            ["name" => "Quimi", "color" => "verde"],
            ["name" => "Sultan", "color" => "blanco"]];

        $result = array_values($ejercicio->filtrarPerros($perros));

        $expected = [
            ["name" => "Turrón", "color" => "blanco"],
            ["name" => "Mr. N", "color" => "blanco"],
            ["name" => "Sultan", "color" => "blanco"]];

        $this->assertEquals($expected, $result);
    }

    /** @test */
    public function ascensorNumerico1()
    {
        $ejercicio = new Ejercicios();

        $ascensor1 = [1, 2, 3, 3, 1];
        $result = $ejercicio->ascensorNumerico($ascensor1);
        $this->assertEquals(false, $result);
    }

    /** @test */
    public function ascensorNumerico2()
    {
        $ejercicio = new Ejercicios();

        $ascensor2 = [1, 2, 5, 4, 3];
        $result = $ejercicio->ascensorNumerico($ascensor2);
        $this->assertEquals(true, $result);

    }

    /** @test */
    public function ascensorNumerico3()
    {
        $ejercicio = new Ejercicios();

        $ascensor3 = [0, 1000, 1];
        $result = $ejercicio->ascensorNumerico($ascensor3);
        $this->assertEquals(true, $result);

    }

    /** @test */
    public function ascensorNumerico4()
    {
        $ejercicio = new Ejercicios();

        $ascensor4 = [2, 1, 3, 1, 1];
        $result = $ejercicio->ascensorNumerico($ascensor4);
        $this->assertEquals(false, $result);

    }

    /** @test */
    public function ascensorNumerico5()
    {
        $ejercicio = new Ejercicios();

        $ascensor5 = [1, 2, 3, 2, 3];
        $result = $ejercicio->ascensorNumerico($ascensor5);
        $this->assertEquals(false, $result);

    }

    /** @test */
    public function ascensorNumerico6()
    {
        $ejercicio = new Ejercicios();

        $ascensor6 = [1, 2, 3, 4, 5];
        $result = $ejercicio->ascensorNumerico($ascensor6);
        $this->assertEquals(false, $result);

    }

    /** @test */
    public function ascensorNumerico7()
    {
        $ejercicio = new Ejercicios();

        $ascensor6 = [1, 2, 100, 20, 3, 3];
        $result = $ejercicio->ascensorNumerico($ascensor6);
        $this->assertEquals(false, $result);

    }
}
