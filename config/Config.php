<?php

namespace App\Config;
define('BASE_PATH', dirname(__FILE__));

class Config
{
    const PATH_TO_SQLITE_FILE = BASE_PATH . '/../database/star_wars.sqlite';
}
