<?php

namespace App\Config;

use PDO;
use PDOException;

/**
 * SQLite connection
 */
class SQLiteConnection
{
    private ?PDO $pdo = null;

    /**
     * Devuelve una instancia de PDO
     * @return PDO
     * @throws
     */
    public function connect(): ?PDO
    {
        try {
            if ($this->pdo == null) {
                $this->pdo = new \PDO("sqlite:" . Config::PATH_TO_SQLITE_FILE, "", "", array(
                    PDO::ATTR_PERSISTENT => true
                ));
            }
            return $this->pdo;
        } catch (PDOException $e) {
            print "Error al abrir db: " . $e->getMessage();
        }
    }
}
