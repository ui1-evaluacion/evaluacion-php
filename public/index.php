<?php
require_once '../vendor/autoload.php';

use App\Config\SQLiteConnection;
$db = (new SQLiteConnection())->connect();

if($_GET['load'] OR $_GET['truncate']) {
    $query = $db->query("DELETE FROM people");

    $query = $db->query("DELETE FROM starships");

    $query = $db->query("DELETE FROM people_starships");
}

?>
<html lang="ES">
<head>
    <link rel="icon" href="./favicon.svg"/>
    <title>Prueba técnica UI1</title>
    <style>
        td,th{
            padding:6px;
        }

        thead tr{
            background-color: burlywood;
            color:white;
        }
        tbody tr:nth-child(even){
            background-color: white;
        }
        tbody tr:nth-child(odd){
            background-color: blanchedalmond;
        }
    </style>
</head>
<body>
<div id="app">
    <script>
        const rellenar = function(){
            window.location = '?load=true';
        }

        const vaciar = function(){
            window.location = '?truncate=true';
        }
    </script>
    <div>
        <button onclick="rellenar()">Rellenar DB</button>
        <button onclick="vaciar()">Vaciar DB</button>
    </div>
    <?php
        if($_GET['load']) {
            $pagina = 1;
            $respuesta = json_decode(file_get_contents('https://swapi.dev/api/people/?page=' . $pagina));
            foreach ($respuesta->results as $persona) {
                $query = $db->query("INSERT INTO people (name,height,birth_year) VALUES ('" . $persona->name . "','" . $persona->height . "','" . $persona->birth_year . "')");
                $p_id = $db->lastInsertId();
                foreach ($persona->starships as $starship_url) {
                    $starship = json_decode(file_get_contents($starship_url));
                    $query = $db->query("INSERT INTO starships (name,model,starship_class) VALUES ('" . $starship->name . "','" . $starship->model . "','" . $starship->starship_class . "')");
                    $st_id = $db->lastInsertId();
                    $query = $db->query("INSERT INTO people_starships (people_id,starships_id) VALUES ('" . $p_id . "','" . $st_id . "')");
                }
            }
            ++$pagina;

            while ($respuesta->next) {
                $respuesta = json_decode(file_get_contents('https://swapi.dev/api/people/?page=' . $pagina));
                foreach ($respuesta->results as $persona) {
                    $query = $db->query("INSERT INTO people (name,height,birth_year) VALUES ('" . $persona->name . "','" . $persona->height . "','" . $persona->birth_year . "')");
                    $p_id = $db->lastInsertId();
                    foreach ($persona->starships as $starship_url) {
                        $starship = json_decode(file_get_contents($starship_url));
                        $query = $db->query("INSERT INTO starships (name,model,starship_class) VALUES ('" . $starship->name . "','" . $starship->model . "','" . $starship->starship_class . "')");
                        $st_id = $db->lastInsertId();
                        $query = $db->query("INSERT INTO people_starships (people_id,starships_id) VALUES ('" . $p_id . "','" . $st_id . "')");
                    }
                }
                ++$pagina;
            }

        }
    ?>
    <table>
        <thead>
           <tr>
               <th>#</th>
               <th>Nombre</th>
               <th>Altura</th>
               <th>Año Nacimiento</th>
               <th>Nombre Nave</th>
               <th>Modelo</th>
               <th>Clase</th>
           </tr>
        </thead>
        <tbody>
            <?php
                $i=1;
                $query = $db->query("SELECT * FROM people");
                while($persona = $query->fetch()){
                    $starships = $db->query("SELECT COUNT(*) as num FROM people_starships WHERE people_id=".$persona['id'])->fetch();
                    if($starships['num']){
                        $query_2 = $db->query("SELECT * FROM people_starships WHERE people_id=".$persona['id']);
                        while($persona_starship = $query_2->fetch()){
                            $query_3 = $db->query("SELECT * FROM starships WHERE id=".$persona_starship['starships_id']);
                            $starship = $query_3->fetch();

            ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $persona['name'] ?></td>
                    <td><?php echo $persona['height'] ?></td>
                    <td><?php echo $persona['birth_year'] ?></td>
                    <td><?php echo $starship['name'] ?></td>
                    <td><?php echo $starship['model'] ?></td>
                    <td><?php echo $starship['starship_class'] ?></td>
                </tr>
            <?php }
                    }else{ ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $persona['name'] ?></td>
                    <td><?php echo $persona['height'] ?></td>
                    <td><?php echo $persona['birth_year'] ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            <?php  }++$i;} ?>
        </tbody>
    </table>
</div>
</body>
</html>
