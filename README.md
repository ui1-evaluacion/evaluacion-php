# Prueba técnica PHP 7.4

Prueba técnica de la **Universidad Isabel I**<p>
Esta prueba consta de 2 partes:

* Ejercicios: 2 ejercicios de lógica con tests unitarios.
* SWAPI

## Requisitos

* Internet
* composer
* PHP
* Cuenta en GitLab (en caso de usar GIT)

## Instalación 💻

```bash
composer install #instalar dependencias
php ./migrations/Init.php #crear tablas de la bdd
php -S localhost:8000 -t ./public #iniciar servidor
```

## Instrucciones

Crea una rama: **PE-nombre-apellido** en https://gitlab.com/ui1-evaluacion/evaluacion-php (en caso de usar GIT)

## Ejercicios

Trata de resolver los métodos que están en `src/Exercices` y realiza los *test unitarios* para comprobar que están
correctamente:

* `ordenarPerros:` Haz una función que devuelva un array con todas los perros que sean de color blanco
  y que además su nombre contenga tanto las letras n OR a (sin importar el orden, las mayúsculas, espacios o acentos).
* `ascensorNumerico:` Recorrer el array de izquierda a derecha y comprueba que los números suben, tienen un punto máximo
  y vuelven a bajar. Devolviendo true o false si lo hace correctamente. En ningún caso deben repetirse los números seguido (ej: 1 , 2 , 2).
  Los números serán siempre de 0 a 1000 de rango

#### Iniciar Tests ✔

```bash
./vendor/bin/phpunit
```

## SWAPI

Este apartado se valorarán, junto con la funcionalidad, las **buenas prácticas** realizadas según tu criterio:
POO, MVC, Patrones de diseño, arquitectura de software, etc..

Para realizar esta práctica te proporcionaremos una BDD con 3 tablas como las siguientes:

![img](public/star_wars_bdd.png)

* `public/index.php`: Tienes una aplicación que importa datos de una API externa. Los datos están en la bdd interna y se
  muestran en pantalla. A partir de esta aplicación pedimos que hagas:
    * Refactorizar el código con las prácticas sugeridas (MVC, POO, etc).
    * Evita que al cargar los datos en la BDD se dupliquen las naves en la tabla starship
    * Permitir ordenar por el campo (starships (name)).
    * Añadir un campo a la tabla people (dark_side(bool)) y poder modificar su valor en la interfaz (true/false) (puedes
      modificar la vista si lo necesitas).
    * Permitir filtrar por el campo dark_side.

> **Nota:** puedes usar frameworks o librerías si lo deseas.
