<?php

namespace App;

class Ejercicios
{
    /**
     * Haz una función que devuelva un array con todas los perros que sean de color blanco
     * y que además su nombre contenga tanto las letras n OR a (sin importar el orden, las mayúsculas, espacios o acentos).
     *
     * $perros = [["name" => "Leo", "color" => "negro"],
     *            ["name" => "Turrón", "color" => "blanco"],
     *            ["name" => "Nevado", "color" => "marrón"],
     *            ["name" => "Mr. N", "color" => "blanco"],
     *            ["name" => "Quimi", "color" => "verde"],
     *            ["name" => "Sultan", "color" => "blanco"]];
     *
     * @param array $perros
     * @return array
     */
    public function filtrarPerros(array $perros): array
    {
        return $perros;
    }

    /**
     * Recorrer el array de izquierda a derecha y comprueba que los números suben, tienen un punto máximo y vuelven a bajar.
     * Devolviendo true o false si lo hace correctamente. En ningún caso deben repetirse los números seguido (ej: 1 , 2 , 2).
     * Los números serán siempre de 0 a 1000 de rango
     *
     * Ej: ascensorNumerico([1, 2, 3, 2, 1]) => true: sube y baja de forma estricta
     *     ascensorNumerico([2, 4, 6, 6, 2]) => false: no sube de forma estricta (se repiten)
     *     ascensorNumerico([1, 2, 3]) => false: sólo sube
     *     ascensorNumerico([0, 1000, 1]) => true
     *
     * @param array $numeros
     * @return bool
     */
    public function ascensorNumerico(array $numeros): bool
    {
        return true;
    }
}


