<?php
require_once dirname(__DIR__) . '/vendor/autoload.php';
use App\Config\SQLiteConnection;

$db = (new SQLiteConnection())->connect();
$stmt = $db->prepare('CREATE TABLE IF NOT EXISTS people (
                                id integer PRIMARY KEY AUTOINCREMENT,
                                name varchar(80),
                                height varchar(80),
                                birth_year varchar(80))');
$stmt->execute();

$stmt = $db->prepare('CREATE TABLE IF NOT EXISTS starships (
                                id integer PRIMARY KEY AUTOINCREMENT,
                                name varchar(80),
                                model varchar(80),
                                starship_class varchar(80))
');
$stmt->execute();

$stmt = $db->prepare('CREATE TABLE IF NOT EXISTS people_starships (
                                id integer PRIMARY KEY AUTOINCREMENT,
                                people_id integer,
                                starships_id integer,
                                FOREIGN KEY(people_id) REFERENCES people(id),
                                FOREIGN KEY(starships_id) REFERENCES starships(id))
');
$stmt->execute();
